package com.dvo;

import java.awt.*;
import javax.swing.*;

/**
 * Created by Danyl on 004 04.07.17.
 */
public class Canvas  extends JComponent {
    public void paintComponent(Graphics g){
        super.paintComponents(g);
        Graphics2D g2d=(Graphics2D)g;

/* 	Устанавливает цвет рисования в зелёный*/
        g2d.setPaint(Color.GREEN);

/* 	Рисует текущим цветом прямоугольник	*/
        g2d.drawRect(100, 100, 80, 20);

        g2d.setPaint(Color.RED);
/* 	Рисует текущим цветом в координатах (150,150) строку "привет мир"*/
        g2d.drawString("Привет мир", 150, 150);

        g2d.setColor(Color.blue);
/*	Рисует текущим цветом овал в координатах (200,50)*/
        g2d.fillOval(200, 50, 50, 20);

/* 	Вызывает обновление себя после завершения рисования	*/
        super.repaint();
    }

}

package com.dvo;

import java.awt.*;
import javax.swing.*;

public class Main {

    public static void main(String[] args) {
        /* Задание заголовка окна*/
        JFrame w = new JFrame("Окно с изображением");
	/*Задание размеров окна*/
        w.setSize(400, 400);

/* 	Если у окна не будет функции закрытия,
 *	при нажатии крестика окно не закроется.*/
        w.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        w.setVisible(true);
    }
}
